#!/bin/bash
DST="public_html"
#COMPRESS="python2 /usr/lib/python2.7/dist-packages/slimmer/slimmer.py"
COMPRESS="yui-compressor"
if [ ! -d "$DST" ]; then mkdir -p "$DST"; fi
for x in {js,css}/*.*; do
    if [ "${x/.min/}" = "$x" ]; then
        #o="$DST/${x/./.min.}"
        o="$DST/$x"
        d="$(dirname "$o")"
        if [ ! -d "$d" ]; then mkdir -p "$d"; fi
        $COMPRESS "$x" > "$o"
    fi
done
cp .htaccess "$DST"
cp chipzirc_pad.xml "$DST"
cp googlef16f6127cab10c88.html "$DST"
cp -r "images" "$DST"
cp -r "screens" "$DST"
php -f "index.php" > "$DST/index.html"
#if [ "$COMPRESS" != "yui-compressor" ]; then
#    $COMPRESS "$DST/index.html" > "$DST/index.temp"
#    mv "$DST/index.temp" "$DST/index.html"
#fi
du -s "$DST"

