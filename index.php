<?php
if(!isset($_SERVER['SERVER_NAME'])) $_SERVER['SERVER_NAME'] = 'chipzirc.net';
if(!isset($_SERVER['REQUEST_URI'])) $_SERVER['REQUEST_URI'] = '';
require_once('lib/Uri.php');
$uri = new \susuka\core\Uri;
$version = 'ChipzIRC-0.100.0.21';
$mirrors = array('http://chipzirc.net/files/', 'http://files.icechip.me/chipzirc/');
$screens = array('01', '02', '03', '04', '05', 'xp_01', 'xp_02', 'xp_03');
function print_dl($ext) {
    global $version, $mirrors;
    $file = $version.'.'.$ext;
    foreach($mirrors as $k => $m) {
        printf('<a href="%s">', htmlspecialchars($m.$file));
        if($k === 0) {
            echo htmlspecialchars($file);        
        } else {
            printf('Mirror %d', $k);
        }
        echo '</a> - ';
    }
}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>ChipzIRC - A free IRC client for Windows</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="keywords" content="chipzirc, wiki, irc, client, internet relay chat, chat, mirc, utf8, utf-8, unicode, C#, windows, free, freeware, download, script, scripting, korean, japanese" />
<meta name="description" content="ChipzIRC - A free IRC client for Windows" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/lightbox.js"></script>
<script type="text/javascript" src="js/detectmobilebrowser.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/lightbox.css" media="screen" />
</head>
<body>
<div id="header" class="clear">
    <div class="content clear">
        <h1><a href="<?php echo htmlspecialchars((string)$uri) ?>">ChipzIRC</a></h1>
        <h2>A free IRC client for Windows</h2>
        <ul id="mainmenu" class="menu">
            <li><a href="#news">News</a></li>
            <li><a href="#downloads">Downloads</a></li>
            <li><a href="#screenshots">Screenshots</a></li>
            <li><a href="#contact">Contact</a></li>
            <li><a href="#donate">Donate</a></li>
        </ul>
    </div>
</div>
<div id="body" class="clear">
    <div class="content clear">
        <noscript class="message error">Please enable JavaScript!</noscript>
        <p class="message success Display_DonateThanks" style="display:none">Thanks for donating!</p>
        <p class="message error Display_DonateCancel" style="display:none">The process was cancelled</p>
        <h1 id="news">News</h1>
        <ul>
            <li>ChipzIRC is a free IRC (Internet Relay Chat) client for Windows (XP/2003/Vista*/7*, x86 and x64).</li>
            <li>ChipzIRC is not in active development. If you're having problems with ChipzIRC, <a href="http://en.wikipedia.org/wiki/Comparison_of_Internet_Relay_Chat_clients">please use one of these IRC clients instead</a>.</li>
        </ul>
        <h1 id="downloads">Downloads</h1>
        <ul>
            <li><?php print_dl('exe')?> Windows installer 1.6MB - 9/3 2009.</li>
            <li><?php print_dl('zip')?> Zip-archive 1.5MB - 9/3 2009.</li>
        </ul>
        <h1 id="screenshots">Screenshots</h1>
        <p>
        <?php foreach($screens as $s) {
            $s = htmlspecialchars($s);
            printf('<a href="screens/%s.png" title="Screenshot %s" rel="lightbox[screens]"><img src="screens/t_%s.png" alt="Screenshot %s" /></a>'.PHP_EOL, $s, $s, $s, $s);
        }
        ?>
        </p>
        <h1 id="contact">Contact</h1>
        <p>Please use the email contact [at] chipzirc.net for feedback and questions.</p>
        <h1 id="donate">Donate</h1>
        <div id="donate-wrap">
        <?php include('lib/paypal.php') ?>
        </div>
    </div>
</div>
<div id="footer" class="clear">
    <div class="content clear">
        <p class="credits">Code & design by chips</p>
        <?php include('lib/gtranslate.php') ?>
    </div>
</div>
</body>
</html>
