<?php


function paypal_button2($return, $cancel, $currency, $img, $lc, $alt) {
	printf('<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
		<input type="hidden" name="cmd" value="_donations" />
		<input type="hidden" name="return" value="%s" />
		<input type="hidden" name="cancel_return" value="%s" />
		<input type="hidden" name="lc" value="%s" />
		<input type="hidden" name="currency_code" value="%s" />
		<input type="hidden" name="business" value="donate@chipzirc.net" />
		<input type="hidden" name="item_name" value="ChipzIRC.net" />
		<input type="hidden" name="item_number" value="ChipzIRC donation" />
		<input type="hidden" name="cn" value="Your email or username" />
		<input type="hidden" name="buyer_credit_promo_code" value="" />
		<input type="hidden" name="buyer_credit_product_category" value="" />
		<input type="hidden" name="buyer_credit_shipping_method" value="" />
		<input type="hidden" name="buyer_credit_user_address_change" value="" />
		<input type="hidden" name="no_shipping" value="1" />
		<input type="hidden" name="tax" value="0" />
		<input type="hidden" name="bn" value="PP-DonationsBF" />
		<input type="image" src="https://www.paypal.com/%s/i/btn/x-click-but04.gif" name="submit" alt="%s" />
		<img alt="" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1" />
	</form>', $return, $cancel, $lc, $currency, $img, $alt ? $alt : 'Make payments with PayPal - it\'s fast, free and secure!');
}

function paypal_button($symbol, $return, $cancel, $currency, $img, $lc, $alt) {
	echo '<div class="button">';
	echo $symbol;
	echo ' ';
	echo $currency;
	echo paypal_button2($return, $cancel, $currency, $img, $lc, $alt);
	echo '</div> ';
}

if(isset($this)) {
	$return = $this->href('', 'DonateThanks');
	$cancel = $this->href('', 'DonateCancel');
} else {
	$return = '';
	$cancel = '';
}

$return = (string)$uri.'#DonateThanks';
$cancel = (string)$uri.'#DonateCancel';

paypal_button('€', $return, $cancel, 'EUR', 'en_GB', 'GB', false);
paypal_button('$', $return, $cancel, 'USD', 'en_US', 'US', false);
paypal_button('£', $return, $cancel, 'GBP', 'en_GB', 'GB', false);
paypal_button('€', $return, $cancel, 'EUR', 'es_ES', 'ES', false);
paypal_button('¥', $return, $cancel, 'JPY', 'ja_JP', 'JP', 'PayPal- オンラインで安全・簡単にお支払い');


