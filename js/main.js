$(function(){
    var mobile = $.browser.mobile || screen.height < 500 || screen.width < 500;
    $('.message').click(function(){
        $(this).hide();
    });
    $('.Display_' + window.location.hash.substring(1)).show();

    $('#mainmenu a').click(function() {
        var href = $(this).attr('href');
        if(href[0] == '#') {
            // FIXME: Doesn't work in androids browser
            $(mobile ? 'body' : '#body').animate({
                 scrollTop: $(href).position().top
            }, 'fast');
            return false;
        }
        return true;
    });
    if(mobile) {
        $('html, body, #body').css({
            overflow: 'visible',
            overflowX: 'visible',
            overflowY: 'visible',
        });
        $('#header, #body, #footer').css({
            position: 'static',
        });
    }
});

